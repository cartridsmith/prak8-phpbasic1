<?php
    //Definisikan konstanta
    define('PHI',3.14);
    define('DBNAME','inventori');
    define('DBSERVER','localhost');

    $r=8;
    $L= PHI * $r*$r;
    $K=2*PHI*$r;

    echo 'Luas lingkaran dengan jari jari '.$r.' : '.$L;
    echo '<br/>Kelilingnya : '.$K;
?>
<hr/>
<?php
    echo ' Nama databasenya : '.DBNAME.'<br/>';
    echo ' Lokasi databasenya ada di : '.DBSERVER;
?>